<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('adminlogin')->name('adminlogin.')->namespace('School')->group(function(){
    Route::name('index')->get('/','AdminController@showLogin');
    Route::name('login')->post('login','AdminController@login');
    Route::name('logout')->get('logout','AdminController@logout');
});

Route::group(['middleware'=>'admin'],function (){
    Route::prefix('admin')->name('admin.')->namespace('School')->group(function(){
        Route::name('index')->get('/','AdminController@index');
        Route::name('create')->post('create','AdminController@create');
        Route::name('edit')->post('{id}/edit','AdminController@update');
        Route::name('delete')->get('{id}/delete','AdminController@delete');
    });
    Route::prefix('year')->name('year.')->namespace('School')->group(function (){
        Route::name('index')->get('/','YearController@index');
        Route::name('create')->post('create','YearController@create');
        Route::name('edit')->post('{id}/edit','YearController@update');
        Route::name('delete')->get('{id}/delete','YearController@delete');
    });
    Route::prefix('batch')->name('batch.')->namespace('School')->group(function (){
        Route::name('index')->get('/','BatchController@index');
        Route::name('create')->post('create','BatchController@create');
        Route::name('edit')->post('{id}/edit','BatchController@update');
        Route::name('delete')->get('{id}/delete','BatchController@delete');
    });
    Route::prefix('major')->name('major.')->namespace('School')->group(function (){
        Route::name('index')->get('/','MajorController@index');
        Route::name('create')->post('create','MajorController@create');
        Route::name('edit')->post('{id}/edit','MajorController@update');
        Route::name('delete')->get('{id}/delete','MajorController@delete');
    });
    Route::prefix('exam')->name('exam.')->namespace('School')->group(function (){
        Route::name('index')->get('/','ExamController@index');
        Route::name('create')->post('create','ExamController@create');
        Route::name('edit')->post('{id}/edit','ExamController@update');
        Route::name('delete')->get('{id}/delete','ExamController@delete');
    });
    Route::prefix('category')->name('category.')->namespace('School')->group(function(){
        Route::name('index')->get('/','CategoryController@index');
        Route::name('create')->post('create','CategoryController@create');
        Route::name('edit')->post('{id}/edit','CategoryController@update');
        Route::name('delete')->get('{id}/delete','CategoryController@delete');
    });
    Route::prefix('subject')->name('subject.')->namespace('School')->group(function (){
        Route::name('index')->get('/','SubjectController@index');
        Route::name('create')->post('create','SubjectController@create');
        Route::name('edit')->post('{id}/edit','SubjectController@update');
        Route::name('delete')->get('{id}/delete','SubjectController@delete');
    });
    Route::prefix('student')->name('student.')->namespace('School')->group(function (){
        Route::name('index')->get('/','StudentController@index');
        Route::name('create')->post('create','StudentController@create');
        Route::name('edit')->post('{id}/edit','StudentController@update');
        Route::name('delete')->get('{id}/delete','StudentController@delete');
        Route::name('detail')->get('{id}/detail','MarkController@detail');
        Route::name('card')->get('{id}/card','StudentController@getcard');
        Route::name('excel')->post('excel','ExcelController@getexcel');
        Route::name('search')->post('search','StudentController@search');
    });
    Route::prefix('mark')->name('mark.')->namespace('School')->group(function(){
        Route::name('index')->get('/','MarkController@index');
        Route::name('create')->post('create','MarkController@create');
        Route::name('edit')->post('{id}/edit','MarkController@update');
        Route::name('delete')->get('{id}/delete','MarkController@delete');
    });
    Route::prefix('excel')->name('excel.')->namespace('School')->group(function(){
        Route::name('downloadExcel')->post('downloadExcel', 'MaatwebsiteController@downloadExcel');
    });
    Route::prefix('pdf')->name('pdf.')->namespace('School')->group(function (){
        Route::name('downloadPdf')->post('{id}/downloadPdf','PdfController@getPdf');
    });
    Route::get('/getimages/{image}','School\StudentController@getImage');
});