$('#modal_edityear').on('show.bs.modal',function (event) {
    var relatedTarget=$(event.relatedTarget);
    var year=relatedTarget.data('year');
    var editurl=relatedTarget.data('editurl');
    $('#year_name').val(year.name);
    $('#editurl').attr('action',editurl);
});
$('#modal_deleteyear').on('show.bs.modal',function (event) {
    var relatedTarget=$(event.relatedTarget);
    var deleteurl=relatedTarget.data('deleteurl');
    $('#deleteurl').attr('action',deleteurl);
});