$('#modal_editcategory').on('show.bs.modal',function (event) {
   var relatedTarget=$(event.relatedTarget);
   var category=relatedTarget.data('category');
   var editurl=relatedTarget.data('editurl');
   $('#category_name').val(category.name);
   $('#editurl').attr('action',editurl);
});
$('#modal_deletecategory').on('show.bs.modal',function (event) {
   var relatedTarget = $(event.relatedTarget);
   var deleteurl=relatedTarget.data('deleteurl');
   $('#deleteurl').attr('action',deleteurl);
});