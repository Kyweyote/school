var data;
$('#modal_issue_expire').on('show.bs.modal',function (event) {
    data = $(event.relatedTarget).data('std');
    console.log(data);
});
$(function(){
    $('.student_card_generate').click(function(e){
        var count = document.getElementById('img-out').childElementCount;
        if (count > 0) {
            $('#img-out').empty();
        }
        $('#student_card_modal').modal('show');
        // var data = $(e.currentTarget).data('std');
        var major = data.major.name;
        var name = data.name;
        var id = data.code;
        var nrc=data.nrc;
        var img = "/getimages/"+data.photo;
        var issue = $('#issue_date').val();
        var expire = $('#expire_date').val();

        $('#card_major').text(major);
        $('#card_name').text(name);
        $('#card_id').text(id);
        $('#card_nrc').text(nrc);
        $('#card_image').attr('src',img);
        $('#card_issue_date').text(issue);
        $('#card_expire_date').text(expire);

        $('#card_major_modal').text(major);
        $('#card_name_modal').text(name);
        $('#card_id_modal').text(id);
        $('#card_nrc_modal').text(nrc);
        $('#card_image_modal').attr('src',img);
        $('#card_issue_date_modal').text(issue);
        $('#card_expire_date_modal').text(expire);

        $('#modal_issue_expire').modal('hide');

        domtoimage.toJpeg(document.getElementById('student_card'), {
            style: {
                padding: 0,
                margin: 0
            }
        })
            .then(function(blob) {
                window.canvasblob = blob;
            });
        // html2canvas($('#student_card'), {
        //    onrendered: function (canvas) {
        //        theCanvas = canvas;
        //        document.getElementById('img-out').appendChild(canvas);
        //    }
        // });
    });
    $('#download').on('click',function(){
        downloadCanvas(this, 'canvas', 'student card.jpg');
    });

    function downloadCanvas(link, canvasId, filename) {
        // var can = $(canvasId);
        link.href = window.canvasblob;
        link.download = filename;
    }
});