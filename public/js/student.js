$('#modal_editstudent').on('show.bs.modal',function (event) {
    var relatedTarget = $(event.relatedTarget);
    var student = relatedTarget.data('student');
    var editurl = relatedTarget.data('editurl');
    var phone = relatedTarget.data('phone');
    $('#student_name').attr('value',student.name);
    $('#student_roll_no').attr('value',student.roll_no);
    $('#student_code').attr('value',student.code);
    $('#student_date_of_birth').attr('value',student.date_of_birth);
    $('#student_father_name').attr('value',student.father_name);
    $('#student_mother_name').attr('value',student.mother_name);
    $('#student_address').attr('value',student.address);
    $('#student_town').attr('value',student.town);
    $('#student_nrc').attr('value',student.nrc);
    $('#student_ph_no').attr('value',phone);
    $('#student_admit_date').val(student.admit_date);
    $('#student_batch').val(student.batch_id);
    $('#student_major').val(student.major_id);
    $('#student_year').val(student.year_id);
    $('#editurl').attr('action',editurl);
});
$('#modal_deletestudent').on('show.bs.modal',function (event) {
    var relatedTarget = $(event.relatedTarget);
    var deleteurl = relatedTarget.data('deleteurl');
    $('#deleteurl').attr('action',deleteurl);
});