$('#modal_editmajor').on('show.bs.modal',function (event) {
    var relatedTarget=$(event.relatedTarget);
    var major=relatedTarget.data('major');
    var editurl=relatedTarget.data('editurl');
    $('#major_name').val(major.name);
    $('#editurl').attr('action',editurl);
});
$('#modal_deletemajor').on('show.bs.modal',function (event) {
    var relatedTarget=$(event.relatedTarget);
    var deleteurl=relatedTarget.data('deleteurl');
    $('#deleteurl').attr('action',deleteurl);
});