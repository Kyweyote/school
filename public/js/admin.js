$('#modal_editadmin').on('show.bs.modal',function (event) {
    var relatedTarget=$(event.relatedTarget);
    var admin=relatedTarget.data('admin');
    var editurl=relatedTarget.data('editurl');
    $('#admin_name').val(admin.name);
    $('#admin_email').val(admin.email);
    $('#editurl').attr('action',editurl);
});
$('#modal_deleteadmin').on('show.bs.modal',function (event) {
    var relatedTarget=$(event.relatedTarget);
    var deleteurl=relatedTarget.data('deleteurl');
    $('#deleteurl').attr('action',deleteurl);
});