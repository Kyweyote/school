$('#modal_editmark').on('show.bs.modal',function (event) {
    var relatedTarget = $(event.relatedTarget);
    var mark = relatedTarget.data('mark');
    var editurl = relatedTarget.data('editurl');
    $('#mark_mark').attr('value',mark.mark);
    $('#mark_exam_id').val(mark.exam_id);
    $('#mark_year_id').val(mark.year_id);
    $('#editurl').attr('action',editurl);
});
$('#modal_deletemark').on('show.bs.modal',function (event) {
    var relatedTarget = $(event.relatedTarget);
    var deleteurl = relatedTarget.data('deleteurl');
    $('#deleteurl').attr('action',deleteurl);
});

$('.roll').on('click',function () {
    var name = $(this).attr('data-name');
    var year = $(this).attr('data-year');
   $(this).parent().parent().children('#searchRoll').val($(this).text());
   $('#student_name').val(name);
   $('#year_name').val(year);
});

$('#searchRoll').on('keyup', function(){
   var filter, li, i;
   filter = this.value.toUpperCase();
   li = $('#roll-menu li');
   for (i = 0; i < li.length; i++){
       if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1){
           li[i].style.display = "";
       }else{
           li[i].style.display = 'none';
       }
   }
});

// For Edit
$('.edit-roll').on('click',function () {
    var name = $(this).attr('data-edit-name');
    var year = $(this).attr('data-edit-year');
    $(this).parent().parent().children('#mark_roll_no').val($(this).text());
    $('#mark_student_name').val(name);
    $('#mark_year').val(year);
});

$('#mark_roll_no').on('keyup', function(){
    var filter, li, i;
    filter = this.value.toUpperCase();
    li = $('#roll-edit-menu li');
    for (i = 0; i < li.length; i++){
        if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1){
            li[i].style.display = "";
        }else{
            li[i].style.display = 'none';
        }
    }
});
//End for Edit

$(document).ready(function () {
    var allOptions = $('#subject_id option')
    $('#category_id').change(function () {
        $('#subject_id option').remove()
        var classN = $('#category_id option:selected').prop('class');
        var opts = allOptions.filter('.' + classN);
        $.each(opts, function (i, j) {
            $(j).appendTo('#subject_id');
        });
    });
});
$(document).ready(function () {
    var allOptions = $('#edit_subject_id option')
    $('#edit_category_id').change(function () {
        $('#edit_subject_id option').remove()
        var classN = $('#edit_category_id option:selected').prop('class');
        var opts = allOptions.filter('.' + classN);
        $.each(opts, function (i, j) {
            $(j).appendTo('#edit_subject_id');
        });
    });
});
$("tr").find("td[data-duplicate-year=true]").not(":first").hide();
$("tr").find("td[data-duplicate-exam=true]").not(":first").hide();