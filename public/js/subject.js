$('#modal_editsubject').on('show.bs.modal',function (event) {
    var relatedTarget = $(event.relatedTarget);
    var subject = relatedTarget.data('subject');
    var editurl = relatedTarget.data('editurl');
    $('#subject_name').val(subject.name);
    $('#subject_code').val(subject.code);
    $('#subject_credit').val(subject.credit);
    $('#subject_category').val(subject.category_id);
    $('#editurl').attr('action',editurl);
});
$('#modal_deletesubject').on('show.bs.modal',function (event) {
    var relatedTarget = $(event.relatedTarget);
    var deleteurl = relatedTarget.data('deleteurl');
    $('#deleteurl').attr('action',deleteurl);
});