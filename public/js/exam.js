$('#modal_editexam').on('show.bs.modal',function (event) {
    var relatedTarget=$(event.relatedTarget);
    var exam=relatedTarget.data('exam');
    var editurl=relatedTarget.data('editurl');
    $('#exam_name').val(exam.name);
    $('#editurl').attr('action',editurl);
});
$('#modal_deleteexam').on('show.bs.modal',function (event) {
   var relatedTarget=$(event.relatedTarget);
   var deleteurl=relatedTarget.data('deleteurl');
   $('#deleteurl').attr('action',deleteurl);
});