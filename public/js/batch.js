$('#modal_editbatch').on('show.bs.modal',function (event) {
    var relatedTarget=$(event.relatedTarget);
    var batch=relatedTarget.data('batch');
    var editurl=relatedTarget.data('editurl');
    $('#batch_name').val(batch.name);
    $('#editurl').attr('action',editurl);
});
$('#modal_deletebatch').on('show.bs.modal',function (event) {
    var relatedTarget=$(event.relatedTarget);
    var deleteurl=relatedTarget.data('deleteurl');
    $('#deleteurl').attr('action',deleteurl);
});