@extends('masteradmin')
@section('title','Exam')
@section('content')

    <div class="list-of-class">
        <p>List of exams</p>
    </div>

    <div class="main-content">
        {{--<div class="select-class">--}}
            {{--<label>Select Class Name</label>--}}
            {{--<select>--}}
                {{--<option>Standard-1</option>--}}
                {{--<option>Standard-2</option>--}}
                {{--<option>Standard-3</option>--}}
                {{--<option>Standard-4</option>--}}
            {{--</select>--}}
        {{--</div>--}}

        {{--<div class="show-entries">--}}
            {{--<label>Show</label>--}}
            {{--<select>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
            {{--</select>--}}
        {{--</div>--}}

        <div class="add-exam">
            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal_addexam">
                <i class="fa fa-plus"></i>&nbspAdd Exam
            </button>
        </div>


        {{--<div class="dropdown print">--}}
            {{--<a href="{{route('excel.downloadExcel')}}"><button class="btn btn-primary" type="button"><i class="fa fa-print"></i>Print--}}
                {{--</button></a>--}}
        {{--</div>--}}

        {{--<div class="search-box">--}}
            {{--<label>Search</label>--}}
            {{--<input class="search">--}}
        {{--</div>--}}

        <table class="table-bordered table-striped table">
            <tr>
                <th><input type="checkbox"></input></th>
                <th>No</th>
                <th>Name</th>
                <th>
                    {{--<select>--}}
                        {{--<option>Select Action</option>--}}
                        {{--<option>Select</option>--}}
                        {{--<option>Select</option>--}}
                        {{--<option>Select</option>--}}
                    {{--</select>--}}
                </th>
            </tr>
            @php($no=1)
                @foreach($exams as $exam)
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>{{$no++}}</td>
                        <td>{{$exam->name}}</td>
                        <td>
                            <button class="delete" data-toggle="modal" data-target="#modal_deleteexam" data-deleteurl="{{route('exam.delete',['id'=>$exam->id])}}"><i class="fa fa-trash-o"></i></button>
                            <button class="edit" data-toggle="modal" data-target="#modal_editexam" data-exam="{{$exam}}" data-editurl="{{route('exam.edit',['id'=>$exam->id])}}"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                @endforeach

        </table>

        {{--<p>Showing 1 to 10 of 17 entries</p>--}}

        {{--<div class="page">--}}
            {{--<ul class="pagination">--}}
                {{--<li class="page-item"><a class="page-link" href="#">Previous</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">Next</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    </div>
@endsection
@section('models')
    @include('exam.create_modal')
    @include('exam.edit_modal')
    @include('exam.delete_modal')
    @endsection
@section('script')
    <script src="{{asset('js/exam.js')}}"></script>
@endsection