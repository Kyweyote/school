<div class="add-path">
    <div class="modal fade" id="modal_addstudent">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Student</h4>
                </div>

                <form method="post" action="{{route('student.create')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="for-year">

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Name</label>
                            <input type="text" class="name-input" id="name" name="name" value="{{old('name')}}">
                            @if($errors->has('name'))
                                <span class="error">{{$errors->first('name')}}</span>
                            @endif
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Roll No</label>
                            <input type="text" class="name-input" id="name" name="roll_no" value="{{old('roll_no')}}">
                            @if($errors->has('roll_no'))
                                <span class="error">{{$errors->first('roll_no')}}</span>
                            @endif
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Student Code</label>
                            <input type="text" class="name-input" id="name" name="code" value="{{old('code')}}">
                            @if($errors->has('code'))
                                <span class="error">{{$errors->first('code')}}</span>
                            @endif
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Date-of-Birth</label>
                            <input type="text" class="name-input" id="name" name="date_of_birth" value="{{old('date_of_birth')}}">
                            @if($errors->has('date_of_birth'))
                                <span class="error">{{$errors->first('date_of_birth')}}</span>
                            @endif
                        </div>


                        <div class="form-group modal-inside" id="name">
                            <label for="name">Father Name</label>
                            <input type="text" class="name-input" id="name" name="father_name" value="{{old('father_name')}}">
                            @if($errors->has('father_name'))
                                <span class="error">{{$errors->first('father_name')}}</span>
                            @endif
                        </div>


                        <div class="form-group modal-inside" id="name">
                            <label for="name">Mother Name</label>
                            <input type="text" class="name-input" id="name" name="mother_name" value="{{old('mother_name')}}">
                            @if($errors->has('mother_name'))
                                <span class="error">{{$errors->first('mother_name')}}</span>
                            @endif
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Town</label>
                            <input type="text" class="name-input" id="name" name="town" value="{{old('town')}}">
                            @if($errors->has('town'))
                                <span class="error">{{$errors->first('town')}}</span>
                            @endif
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Address</label>
                            <input type="text" class="name-input" id="name" name="address" value="{{old('address')}}">
                            @if($errors->has('address'))
                                <span class="error">{{$errors->first('address')}}</span>
                            @endif
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">NRC No</label>
                            <input type="text" class="name-input" id="name" name="nrc" value="{{old('nrc')}}">
                            @if($errors->has('nrc'))
                                <span class="error">{{$errors->first('nrc')}}</span>
                            @endif
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Phone Number</label>
                            <input type="text" class="name-input" id="name" name="ph_no" value="{{old('ph_no')}}">
                            @if($errors->has('ph_no'))
                                <span class="error">{{$errors->first('ph_no')}}</span>
                            @endif
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Date-Admited</label>
                            <input type="" class="datepick" id="date" name="admit_date" value="{{old('admit_date')}}">
                            @if($errors->has('admit_date'))
                                <span class="error">{{$errors->first('admit_date')}}</span>
                            @endif
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Batch</label>
                            <select class="form-group" name="batch_id">
                                @foreach($batches as $batch)
                                    <option value="{{$batch->id}}">{{$batch->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Major</label>
                            <select class="form-group" name="major_id">
                                @foreach($majors as $major)
                                    <option value="{{$major->id}}">{{$major->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Year</label>
                            <select class="form-group" name="year_id">
                                @foreach($years as $year)
                                    <option value="{{$year->id}}">{{$year->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group modal-inside-image" id="name">
                            <label for="name">Student Image Upload</label>
                            <input type="file" name="photo">
                            @if($errors->has('photo'))
                                <span class="error">{{$errors->first('photo')}}</span>
                            @endif
                        </div>

                        <div class="modal-save">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

