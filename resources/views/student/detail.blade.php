@extends('masteradmin')
@section('title','Student')
@section('content')

    {{--<div class="list-of-class">--}}
        {{--<p>List of class Standard-1 Students</p>--}}
    {{--</div>--}}

    <div class="main-content">
        <div class="student-select-class">
            <label>Student Name&emsp;: {{$students->name}}</label>
            <label>Student Roll No : {{$students->roll_no}}</label>
            <label>Student Code &emsp;: {{$students->code}}</label>
        </div>

        {{--<div class="show-entries">--}}
            {{--<label>Show</label>--}}
            {{--<select>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
            {{--</select>--}}
        {{--</div>--}}

        <div class="add-detail-mark">
            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal_addmark">
                <i class="fa fa-plus"></i>&nbspAdd Mark
            </button>
        </div>


        <div class="dropdown print">
            {{--<a href="{{route('pdf.downloadPdf',['id'=>$students->id])}}">--}}
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal_pdfdata">
                    <i class="fa fa-print"></i>Transcript
                </button>
        </div>

        {{--<div class="search-box">--}}
            {{--<label>Search</label>--}}
            {{--<input class="search">--}}
        {{--</div>--}}

        <table class="table-bordered table-striped table">
            <tr>
                {{--<th><input type="checkbox"></input></th>--}}
                <th>No</th>
                <th>Year</th>
                <th>Exam</th>
                <th>Subject</th>
                <th>Mark</th>
                <th>
                    {{--<select>--}}
                        {{--<option>Select Action</option>--}}
                        {{--<option>Select</option>--}}
                        {{--<option>Select</option>--}}
                        {{--<option>Select</option>--}}
                    {{--</select>--}}
                </th>
            </tr>
            @php($no=1)
                @php($year_name='A')
                @php($exam_name='A')
                @foreach($marks as $mark)
                    <tr>
                        {{--<td><input type="checkbox"></td>--}}
                        <td>{{$no++}}</td>
                        @php($year = \App\Models\Year::find($mark->year_id))
                            @if(substr_compare($year_name,$year->name,0)==0)
                                <td></td>
                            @else
                                <td>{{$year->name}}</td>
                            @endif
                        @php($year_name=$year->name)
                        @php($exam = \App\Models\Exam::find($mark->exam_id))
                        {{--<td>{{$exam->name}}</td>--}}
                             @if(substr_compare($exam_name,$exam->name,0)==0)
                                  <td></td>
                             @else
                                  <td>{{$exam->name}}</td>
                             @endif
                        @php($exam_name=$exam->name)
                        @php($subject = \App\Models\Subject::find($mark->subject_id))
                        <td>{{$subject->name}}</td>
                        <td>{{$mark->mark}}</td>
                        <td>
                            {{--<button class="detail" data-excelurl=""><i class="fa fa-eye"></i></button>--}}
                            <button class="delete" data-toggle="modal" data-target="#modal_deletemark" data-deleteurl="{{route('mark.delete',['id'=>$mark->id])}}"><i class="fa fa-trash-o"></i></button>
                            <button class="edit" data-toggle="modal" data-target="#modal_editmark" data-year="" data-student="" data-mark="{{$mark}}" data-editurl="{{route('mark.edit',['id'=>$mark->id])}}"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                @endforeach

        </table>


        <div class="page">
            {{$marks->links()}}
            {{--<ul class="pagination">--}}
            {{--<li class="page-item"><a class="page-link" href="#">Previous</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">1</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">Next</a></li>--}}
            {{--</ul>--}}
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('models')
    @include('mark.create_modal')
    @include('mark.edit_modal')
    @include('mark.delete_modal')
    @include('student.pdfdata_modal');
{{--    @include('mark.pdf_modal')--}}
    @endsection
@section('script')
    <script src="{{asset('js/mark.js')}}"></script>
@endsection