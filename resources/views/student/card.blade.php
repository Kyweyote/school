<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style1.css')}}">
    <title>Document</title>
</head>
<body>
<div class="modal-card">
    <div class="modal-card-header">
        <h4 class="modal-title">SHAN COMMUNITY COLLEDGE</h4>
    </div>
    <div class="modal-card-content">
        <h2><center>STUDENT CARD</center></h2>
        <div class="student-card-data">
            <div class="card-data">
                <label>{{$major->name}}</label>
                <label>Name&emsp;&emsp;&emsp;{{$student->name}}</label>
                <label>Student ID&emsp;{{$student->code}}</label>
                <label>NRC&emsp;&emsp;&emsp;&emsp;-</label>
            </div>
            <img src="{{ URL::to('getimages/'.$student->photo) }}" width="150px" height="170px">
        </div>
    </div>
</div>
</body>
</html>