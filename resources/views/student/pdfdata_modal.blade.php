<div class="add-path">
    <div class="modal fade" id="modal_pdfdata">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">PDF Data</h4>
                </div>

                <form method="post" action="{{route('pdf.downloadPdf',['id'=>$students->id])}}">
                    {{csrf_field()}}
                    <div class="for-year">
                        <div class="form-group modal-inside" id="name">
                            <label for="name">University</label>
                            <input type="text" class="name-input" id="name" name="uni" value="{{old('uni')}}">
                            @if($errors->has('uni'))
                                <span class="error">{{$errors->first('uni')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Address</label>
                            <input type="text" class="name-input" id="name" name="address" value="{{old('address')}}">
                            @if($errors->has('address'))
                                <span class="error">{{$errors->first('address')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Phone Number</label>
                            <input type="text" class="name-input" id="name" name="ph_no" value="{{old('ph_no')}}">
                            @if($errors->has('ph_no'))
                                <span class="error">{{$errors->first('ph_no')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Date</label>
                            <input type="" class="datepick" id="name" name="date" value="{{old('date')}}">
                            @if($errors->has('date'))
                                <span class="error">{{$errors->first('date')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Rector Name</label>
                            <input type="text" class="name-input" id="name" name="rector" value="{{old('rector')}}">
                            @if($errors->has('rector'))
                                <span class="error">{{$errors->first('rector')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Rector Positon</label>
                            <input type="text" class="name-input" id="name" name="position" value="{{old('position')}}">
                            @if($errors->has('position'))
                                <span class="error">{{$errors->first('position')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Rector Signature Date</label>
                            <input type="text" class="datepick" id="name" name="signature_date" value="{{old('signature_date')}}">
                            @if($errors->has('signature_date'))
                                <span class="error">{{$errors->first('signature_date')}}</span>
                            @endif
                        </div>

                        <div class="modal-save">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

