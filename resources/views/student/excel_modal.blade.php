<div class="add-path">
    <div class="modal fade" id="modal_excel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Excel Sheet</h4>
                </div>

                <form method="post" action="{{route('excel.downloadExcel')}}">
                    {{csrf_field()}}
                    <div class="for-year">
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Year</label>
                            <select class="form-group" name="year">
                                @foreach($years as $year)
                                    <option value="{{$year->id}}">{{$year->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Major</label>
                            <select class="form-group" name="major">
                                @foreach($majors as $major)
                                    <option value="{{$major->id}}">{{$major->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="modal-save">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

