@extends('masteradmin')
@section('title','Student')
@section('content')
        <div class="list-of-class">
            <p>List of Students</p>
        </div>

        <div class="main-content">
            {{--<div class="select-class">--}}
                {{--<label>Select Class Name</label>--}}
                {{--<select>--}}
                    {{--<option>Standard-1</option>--}}
                    {{--<option>Standard-2</option>--}}
                    {{--<option>Standard-3</option>--}}
                    {{--<option>Standard-4</option>--}}
                {{--</select>--}}
            {{--</div>--}}

            {{--<div class="show-entries">--}}
                {{--<label>Show</label>--}}
                {{--<select>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                    {{--<option>10</option>--}}
                {{--</select>--}}
            {{--</div>--}}

            <div class="add-exam">
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal_addstudent">
                    <i class="fa fa-plus"></i>&nbspAdd Student
                </button>
            </div>


            <div class="dropdown print">
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal_excel">
                    <i class="fa fa-print"></i>Excel
                </button>
            </div>

            <form method="post" action="{{route('student.search')}}">
                {{csrf_field()}}
            <div class="student-search-box">
                <label>Search</label>
                <input class="search" type="text" name="word">
                <button type="submit" class="search-button"><i class="fa fa-search"></i></button>
            </div>
            </form>

            <table class="table-bordered table-striped table">
                <tr>
                    <th><input type="checkbox"></input></th>
                    <th>No</th>
                    <th>Name</th>
                    <th>Roll No</th>
                    <th>Code</th>
                    <th>
                        {{--<select>--}}
                            {{--<option>Select Action</option>--}}
                            {{--<option>Select</option>--}}
                            {{--<option>Select</option>--}}
                            {{--<option>Select</option>--}}
                        {{--</select>--}}
                    </th>
                </tr>
                @php($no=1)
                    @foreach($students as $student)
                        <tr>
                            <td><input type="checkbox"></td>
                            <td>{{$no++}}</td>
                            <td>{{$student->name}}</td>
                            <td>{{$student->roll_no}}</td>
                            <td>{{$student->code}}</td>
                            <td><a href="{{route('student.detail',['id'=>$student->id])}}"><button class="detail"><i class="fa fa-eye"></i></button></a>
                                    @php($phone = $student->phones->implode('ph_no',','))
                                <button class="delete" data-toggle="modal" data-target="#modal_deletestudent" data-phone="" data-deleteurl="{{route('student.delete',['id'=>$student->id])}}"><i class="fa fa-trash-o"></i></button>
                                <button class="edit" data-toggle="modal" data-target="#modal_editstudent" data-phone="{{$phone}}" data-student="{{$student}}" data-editurl="{{route('student.edit',['id'=>$student->id])}}"><i class="fa fa-pencil"></i></button>
                                {{--<button data-std="{{$student}}" class="delete student_card_generate" id="student_card_generate"><i class="fa fa-users"></i></button>--}}
                                <button data-std="{{$student}}" data-toggle="modal" class="delete modal_issue_expire" data-target="#modal_issue_expire"><i class="fa fa-users"></i></button>
                            </td>
                        </tr>
                    @endforeach

            </table>

            {{--<p>hi</p>--}}

            <div class="page">
                {{$students->links()}}
                {{--<ul class="pagination">--}}
                    {{--<li class="page-item"><a class="page-link" href="#">Previous</a></li>--}}
                    {{--<li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                    {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                    {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                    {{--<li class="page-item"><a class="page-link" href="#">Next</a></li>--}}
                {{--</ul>--}}
            </div>
            <div class="clearfix"></div>

        </div>
@endsection
@section('models')
    @include('student.create_modal')
    @include('student.edit_modal')
    @include('student.delete_modal')
    @include('student.excel_modal')
    {{--Start of card-modal--}}
        <div class="modal fade" id="modal_issue_expire">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Date for Student Card</h4>
                    </div>

                        <div class="for-year">
                            <div class="form-group modal-inside" id="name">
                                <label for="name">Issue Date</label>
                                <input type="" class="datepick" id="issue_date" name="issue_date" value="{{old('issue_date')}}">
                                @if($errors->has('issue_date'))
                                    <span class="error">{{$errors->first('issue_date')}}</span>
                                @endif
                            </div>
                            <div class="form-group modal-inside" id="name">
                                <label for="name">Expiry Date</label>
                                <input type="" class="datepick" id="expire_date" name="expire_date" value="{{old('expire_date')}}">
                                @if($errors->has('expire_date'))
                                    <span class="error">{{$errors->first('expire_date')}}</span>
                                @endif
                            </div>

                            <div class="modal-save">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary student_card_generate" id="student_card_generate">Save</button>
                            </div>
                        </div>
                </div>
            </div>

        </div>

    {{--End of card-modal--}}

    <div class="modal fade" id="student_card_modal" role="dialog">
        <div>
            <div class="modal-content" id="generated_card">
                <div class="modal-card">

                    <div class="modal-card-header">
                        <img src="{{ URL::to('/') }}/logo/school_logo.png" width="50px" height="50px">
                        <h5 class="modal-title">ၸၼ့်ၸွမ်တူင်ႇၵူၼ်းတႆ</h5>
                        <h5 class="modal-title">SHAN COMMUNITY COLLEGE</h5>
                    </div>
                    <div class="modal-card-content">
                        <h4><center>STUDENT CARD</center></h4>
                        <div class="student-card-data">
                            <div class="card-data">
                                <label id="card_major_modal"></label>
                                <label>Name&emsp;&emsp;<span id="card_name_modal"></span></label>
                                <label>Student ID&emsp;<span id="card_id_modal"></span></label>
                                <label>NRC&emsp;&emsp;<span id="card_nrc_modal"></span></label>
                            </div>
                            <img id="card_image_modal" width="116px" height="125px">
                        </div>
                        <div class="card-date">
                            <label id="issue_date_label">Issued on <span id="card_issue_date_modal"></span></label>
                            <label id="expire_date_label">Expiry on <span id="card_expire_date_modal"></span></label>
                        </div>
                    </div>
                </div>
                <div class="card-logout-modal">
                <button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                <a id="download"><button type="submit" class="btn btn-primary">Download</button></a>
                <div id="img-out"></div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('img')
    <div id="student_card" class="modal-card" style="background-color: white">
        <div class="modal-card-header">
            <img src="{{ URL::to('/') }}/logo/school_logo.png" width="50px" height="50px">
            <h5 class="modal-title">ၸၼ့်ၸွမ်တူင်ႇၵူၼ်းတႆ</h5>
            <h5 class="modal-title">SHAN COMMUNITY COLLEGE</h5>
        </div>
        <div class="modal-card-content">
            <h4><center>STUDENT CARD</center></h4>
            <div class="student-card-data">
                <div class="card-data">
                    <label id="card_major"></label>
                    <label>Name&emsp;&emsp;&emsp;<span id="card_name"></span></label>
                    <label>Student ID&emsp;<span id="card_id"></span></label>
                    <label>NRC&emsp;&emsp;<span id="card_nrc"></span></label>
                </div>
                <img id="card_image" width="116px" height="125px">
            </div>
            <div class="card-date">
                <label id="issue_date_label">Issued on <span id="card_issue_date"></span></label>
                <label id="expire_date_label">Expiry on <span id="card_expire_date"></span></label>
            </div>
        </div>
    </div>

    <div id="img-out"></div>
@stop
@section('script')
    <script src="{{asset('js/student.js')}}"></script>
    <script src="{{asset('js/dom2img.js')}}"></script>
    <script src="{{asset('js/html2canvas.js')}}"></script>
    <script src="{{asset('js/student-card.js')}}"></script>
@endsection