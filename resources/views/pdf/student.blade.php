<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{--<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">--}}
    <title>TRANSCRIPT</title>
    <style>
        @font-face {
            font-family: mmrtxt;
            src: url('{{public_path("/fonts/mmrtext.ttf")}}')  format('truetype');
        }

        .unicode {
            font-family: mmrtxt;
        }
        {{--body {--}}
            {{--font-family: mmrtxt;--}}
        {{--}--}}
        /*.pdf{*/
            /*margin-left:-100px;*/
            /*padding-left:-100px;*/
        /*}*/

        .pdf-header{
            display: inline;
            margin-top:5px;
            /*margin-left:-00px;*/
        }
        .pdf-header-logo{
            width:10%;
            float: left;
            /*padding-right:0px;*/
        }
        img{
            width:100%;
            margin-left: 20px;
            float: left;
        }

        #watermark {
            position: fixed;

            /**
                Set a position in the page for your image
                This should center it vertically
            **/
            opacity: 0.15;
            bottom:   8cm;
            left:     5.5cm;

            /** Change image dimensions**/
            width:    8cm;
            height:   8cm;

            /** Your watermark should be behind every content**/
            z-index:  -1000;
        }

        {{--.pdf::after {--}}
            {{--content: "";--}}
            {{--background:url("{{ URL::to('/')}}/logo/school_logo.png");--}}
            {{--position: fixed;--}}
            {{--opacity: 0.1;--}}
            {{--left: 5.5cm;--}}
            {{--bottom: 10cm;--}}
            {{--width: 8cm;--}}
            {{--height: 8cm;--}}
            {{--z-index: -1000;--}}
        {{--}--}}
        .pdf-header h4,h5{
            font-weight: bold;
            text-align: center;
        }
        .pdf-student{
            margin-top:5px;
            width:40%;
            float:left;
        }
        .pdf-student label{
            display: block;
            padding-top:5px;
            font-size:12px;
            margin-left:10px;
        }
        .pdf-faculty{
            margin-top:5px;
            width:40%;
            float: right;
        }
        .pdf-faculty label{
            display: block;
            padding-top:5px;
            font-size:12px;
            margin-left:15px;
        }
        .pdf-transcript{
            font-size: 12px;
            float:left;
            display:block;
            margin-top:20px;
            margin-left:10px;
        }
        table{
            border-collapse:collapse;

        }
        .pdf-transcript table th,td{
            /*border: 1px solid black;*/
            padding-right:15px;
            text-align: left;
            margin-right:10px;
            page-break-inside: avoid !important;
        }
        .pdf-transcript h4{
            padding-top:0px;
            padding-bottom:0px;
            margin-left:0px;
        }
        .pdf-code{
            width:50px;
            text-align: left;
        }
        .pdf-subject{
            margin-left:100px;
            width:200px;
            text-align: left;
        }
        .pdf-mark{
            width:15px;
            text-align: left;
        }
        .pdf-grade{
            width:15px;
            text-align: left;
        }
        .pdf-gpa{
            padding-right:110px;
            font-size:12px;
            text-align: left;
        }
        .total-gpa span{
            font-size:12px;
            display: block;
        }
        .pdf-right-side{
            float: right;
            margin-right: 20px;
            width:300px;
        }
        .pdf-right-side span{
            text-align: center;
            font-size:14px;
            display: block;
        }
        #horizontal-line{
            margin-top:100px;
            width:100%;
            height:1px;
            background-color: black;
        }
        #horizontal-bottom-line{
            width:100%;
            height:1px;
            background-color: black;
        }
        #vertical-line{
            margin-left: 50%;
            width:1px;
            height:675px;
            background-color:black;
        }
        .footer label{
            display: block;
            text-align: center;
            color: red;
        }
        .footer-first {
            font-size: 10px;
        }
        .footer-second{
            font-size: 8px;
        }
    </style>
</head>
<body>
<div class="pdf">
    <div class="pdf-header">
            <div class="pdf-header-logo"><img src="{{public_path('/logo/school_logo.png')}}" height="70px" width="70px" /></div>
            <strong><center><span class="unicode">{{$uni}}</span></center></strong>
            <center><span>{{$address}} {{$ph_no}}</span></center>
            <h5>OFFICIAL TRANSCRIPT</h5>
    </div>
    <div class="pdf-student">
        <label>NAME :{{$student_name}}</label>
        <label>STUDENT CODE : <strong>{{$student->code}}</strong></label>
        <label>DATE : <strong>{{date('F d,Y', strtotime($date))}}</strong></label>
        <label>MAJOR : <strong>{{$student->major->name}}</strong></label>
        <label>DEGREE</label>
    </div>
    <div class="pdf-faculty">
        <label>DATE ADMITED : <strong>{{date('F d,Y', strtotime($student->admit_date))}}</strong></label>
        <label><br></label>
        <label><br></label>
        <label><br></label>
        <label>DATE GRANTED</label>
    </div>
    <div id="horizontal-line"></div>
    <div class="pdf-transcript">
        @php($year_name='a')
        @php($exam_name='a')
        @php($gpa_sum=0)
        @php($credit_sum=0)
        @php($total_credit=0)
        @php($total_gpa=0)
        @php($exam_count=0)
        @php($line_count=0)
        @foreach($orders as $order)
               @php($year = \App\Models\Year::find($order->year_id))
               @php($exam = \App\Models\Exam::find($order->exam_id))
               @php($exam_show_count=\App\Models\Mark::where('year_id',$year->id)->where('exam_id',$exam->id)->where('student_id',$order->student_id)->count())
               @if($exam_count<$exam_show_count)
                      @php($exam_count++)
                      @if ($exam_count == $exam_show_count)

                            <table>
                                {{--<tr>--}}
                                {{--<th class="pdf-code"></th>--}}
                                {{--@php($year = \App\Models\Year::find($order->year_id))--}}
                                {{--@php($exam = \App\Models\Exam::find($order->exam_id))--}}
                                {{--@if(substr_compare($year_name,$year->name,0)==0 && substr_compare($exam_name,$exam->name,0)==0)--}}
                                {{--<th></th>--}}
                                {{--@else--}}
                                {{--<th class="pdf-subject">{{$year->name}} , {{$exam->name}}</th>--}}
                                {{--@endif--}}
                                {{--@php($year_name=$year->name)--}}
                                {{--@php($exam_name=$exam->name)--}}
                                {{--<th class="pdf-mark"></th>--}}
                                {{--<th class="pdf-grade"></th>--}}
                                {{--</tr>--}}
                                    @php($gpa)
                                        @php($subject = \App\Models\Subject::find($order->subject_id))
                                            <tr>
                                                <td class="pdf-code">{{$subject->code}}</td>
                                                <td class="pdf-subject">{{$subject->name}}</td>
                                                <td class="pdf-mark">{{$subject->credit}}</td>
                                                @php($credit_sum+=$subject->credit)
                                                @if($order->mark>79)
                                                @php($gpa_sum+=4.00)
                                                @elseif($order->mark>74)
                                                @php($gpa_sum+=3.50)
                                                @elseif($order->mark>69)
                                                @php($gpa_sum+=3.00)
                                                @elseif($order->mark>64)
                                                @php($gpa_sum+=2.50)
                                                @elseif($order->mark>59)
                                                @php($gpa_sum+=2.00)
                                                @elseif($order->mark>54)
                                                @php($gpa_sum+=1.50)
                                                @elseif($order->mark>49)
                                                @php($gpa_sum+=1.00)
                                                @else
                                                @php($gpa_sum+=0)
                                                @endif

                                                @if($order->mark>79)
                                                    <td class="pdf-grade"> {{'A'}}</td>
                                                @elseif($order->mark>74)
                                                    <td class="pdf-grade"> {{'B+'}}</td>
                                                @elseif($order->mark>69)
                                                    <td class="pdf-grade"> {{'B'}}</td>
                                                @elseif($order->mark>64)
                                                    <td class="pdf-grade"> {{'C+'}}</td>
                                                @elseif($order->mark>59)
                                                    <td class="pdf-grade"> {{'C'}}</td>
                                                @elseif($order->mark>54)
                                                    <td class="pdf-grade"> {{'D+'}}</td>
                                                @elseif($order->mark>49)
                                                    <td class="pdf-grade"> {{'D'}}</td>
                                                @else
                                                    <td class="pdf-grade"> {{'F'}}</td>
                                                @endif

                                                </tr>
                                </table>
                                                    <span class="pdf-gpa">{{$student->code}} </span>
                                                    <span class="pdf-gpa">{{$credit_sum}} </span>
                                                    <span class="pdf-gpa">{{number_format($gpa_sum/$exam_show_count,2) }}</span>
                            @php($total_credit+=$credit_sum)
                            @php($total_gpa+=$gpa_sum)
                            @php($exam_count=0)
                            @php($gpa_sum=0)
                            @php($credit_sum=0)
                      @else
                            @if(substr_compare($year_name,$year->name,0)==0 && substr_compare($exam_name,$exam->name,0)==0)
                                <label></label>
                            @else
                                <h4><strong>{{$exam->name}}</strong></h4>
                            @endif
                            @php($year_name=$year->name)
                            @php($exam_name=$exam->name)

                            <table>
                                {{--<tr>--}}
                                    {{--<th class="pdf-code"></th>--}}
                                    {{--@php($year = \App\Models\Year::find($order->year_id))--}}
                                        {{--@php($exam = \App\Models\Exam::find($order->exam_id))--}}
                                            {{--@if(substr_compare($year_name,$year->name,0)==0 && substr_compare($exam_name,$exam->name,0)==0)--}}
                                                {{--<th></th>--}}
                                            {{--@else--}}
                                                {{--<th class="pdf-subject">{{$year->name}} , {{$exam->name}}</th>--}}
                                            {{--@endif--}}
                                        {{--@php($year_name=$year->name)--}}
                                            {{--@php($exam_name=$exam->name)--}}
                                            {{--<th class="pdf-mark"></th>--}}
                                            {{--<th class="pdf-grade"></th>--}}
                                {{--</tr>--}}
                                @php($gpa)
                                @php($subject = \App\Models\Subject::find($order->subject_id))
                                <tr>
                                        <td class="pdf-code">{{$subject->code}}</td>
                                        <td class="pdf-subject">{{$subject->name}}</td>
                                        <td class="pdf-mark">{{$subject->credit}}</td>
                                        @php($credit_sum+=$subject->credit)
                                        @if($order->mark>79)
                                            @php($gpa_sum+=4.00)
                                        @elseif($order->mark>74)
                                            @php($gpa_sum+=3.50)
                                        @elseif($order->mark>69)
                                            @php($gpa_sum+=3.00)
                                        @elseif($order->mark>64)
                                            @php($gpa_sum+=2.50)
                                        @elseif($order->mark>59)
                                            @php($gpa_sum+=2.00)
                                        @elseif($order->mark>54)
                                            @php($gpa_sum+=1.50)
                                        @elseif($order->mark>49)
                                            @php($gpa_sum+=1.00)
                                        @else
                                            @php($gpa_sum+=0)
                                        @endif

                                        @if($order->mark>79)
                                            <td class="pdf-grade"> {{'A'}}</td>
                                            @elseif($order->mark>74)
                                            <td class="pdf-grade"> {{'B+'}}</td>
                                            @elseif($order->mark>69)
                                            <td class="pdf-grade"> {{'B'}}</td>
                                            @elseif($order->mark>64)
                                            <td class="pdf-grade"> {{'C+'}}</td>
                                            @elseif($order->mark>59)
                                            <td class="pdf-grade"> {{'C'}}</td>
                                            @elseif($order->mark>54)
                                            <td class="pdf-grade"> {{'D+'}}</td>
                                            @elseif($order->mark>49)
                                            <td class="pdf-grade"> {{'D'}}</td>
                                            @else
                                            <td class="pdf-grade"> {{'F'}}</td>
                                            @endif

                                </tr>
                            </table>
                      @endif
              @endif
            @endforeach
            <div class="total-gpa">
                <span>~ END OF RECORD ~</span>
                <span>TOTAL CREDITS EARNED = {{$total_credit}}</span>
                <span>GRADE POINTS AVERAGE = {{number_format($total_gpa/$orders_count,2)}}</span>
            </div>
            {{--<div class="pdf-right-side">--}}
                {{--<span>TOTAL CREDITS EARNED = {{$total_credit}}</span>--}}
                {{--<span>GRADE POINTS AVERAGE = {{number_format($total_gpa/$orders_count,2)}}</span>--}}
                {{--<br><br><br><br>--}}
                {{--<span>{{$rector}}</span>--}}
                {{--<span>{{$position}}</span>--}}
                {{--<span>{{date('F d,Y', strtotime($signature_date))}}</span>--}}
            {{--</div>--}}
    </div>

    <div class="pdf-right-side">
        {{--<span>TOTAL CREDITS EARNED = {{$total_credit}}</span>--}}
        {{--<span>GRADE POINTS AVERAGE = {{number_format($total_gpa/$orders_count,2)}}</span>--}}
        <br><br><br><br>
        <span class="unicode">{{$rector}}</span>
        <span class="unicode">{{$position}}</span>
        <span>{{date('F d,Y', strtotime($signature_date))}}</span>
    </div>
    <div id="vertical-line"></div>
    <div id="horizontal-bottom-line"></div>
    <div class="footer">
        <label class="footer-first">OFFICIAL TRANSCRIPT IS NOT VALID WITHOUT SEM AND ORIGINAL SIGNATURE</label>
        <label class="footer-second">Warning is published and against the central law</label>
    </div>
</div>
<div id="watermark">
    <img src="{{public_path('/logo/school_logo.png')}}" height="100%" width="100%" />
</div>
</body>
</html>