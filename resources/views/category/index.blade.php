@extends('masteradmin')
@section('title','Category')
@section('content')
    <div class="list-of-class">
        <p>List of class Subject-Categories</p>
    </div>

    <div class="main-content">
        {{--<div class="select-class">--}}
            {{--<label>Select Class Name</label>--}}
            {{--<select>--}}
                {{--<option>Standard-1</option>--}}
                {{--<option>Standard-2</option>--}}
                {{--<option>Standard-3</option>--}}
                {{--<option>Standard-4</option>--}}
            {{--</select>--}}
        {{--</div>--}}

        {{--<div class="show-entries">--}}
            {{--<label>Show</label>--}}
            {{--<select>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
            {{--</select>--}}
        {{--</div>--}}

        <div class="add-student">
            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal_addcategory">
                <i class="fa fa-plus"></i>&nbspAdd category
            </button>
        </div>

        {{--<div class="dropdown print">--}}
            {{--<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-print"></i>Print--}}
                {{--<span class="caret"></span></button>--}}
            {{--<ul class="dropdown-menu printdropdown">--}}
                {{--<li><a href="#">A4</a></li>--}}
                {{--<li><a href="#">A5</a></li>--}}
                {{--<li><a href="#">A6</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}

        {{--<div class="search-box">--}}
            {{--<label>Search</label>--}}
            {{--<input class="search">--}}
        {{--</div>--}}

        <table class="table-bordered table-striped table">
            <tr>
                <th><input type="checkbox"></input></th>
                <th>No</th>
                <th>Name</th>
                <th>
                    {{--<select>--}}
                        {{--<option>Select Action</option>--}}
                        {{--<option>Select</option>--}}
                        {{--<option>Select</option>--}}
                        {{--<option>Select</option>--}}
                    {{--</select>--}}
                </th>
            </tr>
            @php($no=1)
                @foreach($categories as $category)
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>{{$no++}}</td>
                        <td>{{$category->name}}</td>
                        <td>
                            <button class="delete" data-toggle="modal" data-target="#modal_deletecategory" data-deleteurl="{{route('category.delete',['id'=>$category->id])}}"><i class="fa fa-trash-o"></i></button>
                            <button class="edit" data-toggle="modal" data-target="#modal_editcategory" data-category="{{$category}}" data-editurl="{{route('category.edit',['id'=>$category->id])}}"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                @endforeach

        </table>

        {{--<p>Showing 1 to 10 of 17 entries</p>--}}

        {{--<div class="page">--}}
            {{--<ul class="pagination">--}}
                {{--<li class="page-item"><a class="page-link" href="#">Previous</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">Next</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    </div>
@endsection
@section('models')
    @include('category.create_modal')
    @include('category.edit_modal')
    @include('category.delete_modal')
    @endsection
@section('script')
    <script src="{{asset('js/category.js')}}"></script>
@endsection