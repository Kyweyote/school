<div class="add-path">
    <div class="modal fade" id="modal_editcategory">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Category</h4>
                </div>

                <form method="post" id="editurl">
                    {{csrf_field()}}
                    <div class="for-year">
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Name</label>
                            <input type="text" class="name-input" id="category_name" name="name">
                            @if($errors->has('name'))
                                <span class="error">{{$errors->first('name')}}</span>
                            @endif
                        </div>

                        <div class="modal-save">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

