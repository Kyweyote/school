<div class="add-path">
    <div class="modal fade" id="modal_addsubject">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Category</h4>
                </div>

                <form method="post" action="{{route('subject.create')}}">
                    {{csrf_field()}}
                    <div class="for-year">
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Name</label>
                            <input type="text" class="name-input" id="name" name="name" value="{{old('name')}}">
                            @if($errors->has('name'))
                                <span class="error">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Subject-Code</label>
                            <input type="text" class="name-input" id="name" name="code" value="{{old('code')}}">
                            @if($errors->has('code'))
                                <span class="error">{{$errors->first('code')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Subject-Credit</label>
                            <input type="text" class="name-input" id="name" name="credit" value="{{old('credit')}}">
                            @if($errors->has('credit'))
                                <span class="error">{{$errors->first('credit')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Category</label>
                            <select class="form-group" name="category">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                            </select>
                        </div>

                        <div class="modal-save">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

