<div class="add-path">
    <div class="modal fade" id="modal_editsubject">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Category</h4>
                </div>

                <form method="post" id="editurl">
                    {{csrf_field()}}
                    <div class="for-year">
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Name</label>
                            <input type="text" class="name-input" id="subject_name" name="name">
                            @if($errors->has('name'))
                                <span class="error">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Subject-Code</label>
                            <input type="text" class="name-input" id="subject_code" name="code">
                            @if($errors->has('code'))
                                <span class="error">{{$errors->first('code')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Subject-Credit</label>
                            <input type="text" class="name-input" id="subject_credit" name="credit">
                            @if($errors->has('credit'))
                                <span class="error">{{$errors->first('credit')}}</span>
                            @endif
                        </div>
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Category</label>
                            <select class="form-group" name="category" id="subject_category">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="modal-save">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

