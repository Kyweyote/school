@extends('masteradmin')
@section('title','Subject')
@section('content')
    <div class="list-of-class">
        <p>List of Subjects</p>
    </div>

    <div class="main-content">
        {{--<div class="select-class">--}}
            {{--<label>Select Class Name</label>--}}
            {{--<select>--}}
                {{--<option>Standard-1</option>--}}
                {{--<option>Standard-2</option>--}}
                {{--<option>Standard-3</option>--}}
                {{--<option>Standard-4</option>--}}
            {{--</select>--}}
        {{--</div>--}}

        {{--<div class="show-entries">--}}
            {{--<label>Show</label>--}}
            {{--<select>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
                {{--<option>10</option>--}}
            {{--</select>--}}
        {{--</div>--}}

            <div class="add-student">
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal_addsubject">
                    <i class="fa fa-plus"></i>&nbspAdd subject
                </button>
            </div>

        {{--<div class="dropdown print">--}}
            {{--<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-print"></i>Print--}}
                {{--<span class="caret"></span></button>--}}
            {{--<ul class="dropdown-menu printdropdown">--}}
                {{--<li><a href="#">A4</a></li>--}}
                {{--<li><a href="#">A5</a></li>--}}
                {{--<li><a href="#">A6</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}

        {{--<div class="search-box">--}}
            {{--<label>Search</label>--}}
            {{--<input class="search">--}}
        {{--</div>--}}

            <table class="table-bordered table-striped table">
                <tr>
                    <th><input type="checkbox"></input></th>
                    <th>No</th>
                    <th>Name</th>
                    <th>Subject Code</th>
                    <th>Subject Credit</th>
                    <th>
                        {{--<select>--}}
                            {{--<option>Select Action</option>--}}
                            {{--<option>Select</option>--}}
                            {{--<option>Select</option>--}}
                            {{--<option>Select</option>--}}
                        {{--</select>--}}
                    </th>
                </tr>
                @php($no=1)
                    @foreach($subjects as $subject)
                        <tr>
                            <td><input type="checkbox"></td>
                            <td>{{$no++}}</td>
                            <td>{{$subject->name}}</td>
                            <td>{{$subject->code}}</td>
                            <td>{{$subject->credit}}</td>
                            <td>
                                <button class="delete" data-toggle="modal" data-target="#modal_deletesubject" data-deleteurl="{{route('subject.delete',['id'=>$subject->id])}}"><i class="fa fa-trash-o"></i></button>
                                <button class="edit" data-toggle="modal" data-target="#modal_editsubject" data-subject="{{$subject}}" data-editurl="{{route('subject.edit',['id'=>$subject->id])}}"><i class="fa fa-pencil"></i></button>
                            </td>
                        </tr>
                    @endforeach

            </table>

        {{--<p>Showing 1 to 10 of 17 entries</p>--}}

        {{--<div class="page">--}}
            {{--<ul class="pagination">--}}
                {{--<li class="page-item"><a class="page-link" href="#">Previous</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">Next</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    </div>
@endsection
@section('models')
    @include('subject.create_modal')
    @include('subject.edit_modal')
    @include('subject.delete_modal')
    @endsection
@section('script')
    <script src="{{asset('js/subject.js')}}"></script>
@endsection