<div class="add-path">
    <div class="modal fade" id="modal_addbatch">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Batch</h4>
                </div>

                <form method="post" action="{{route('batch.create')}}">
                    {{csrf_field()}}
                    <div class="for-year">
                        <div class="form-group modal-inside" id="name">
                            <label for="name">Name</label>
                            <input type="text" class="name-input" id="name" name="name" value="{{old('name')}}">
                            @if($errors->has('name'))
                                <span class="error">{{$errors->first('name')}}</span>
                            @endif
                        </div>

                        <div class="modal-save">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

