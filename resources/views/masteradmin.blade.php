<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script language="javascript" src="https://momentjs.com/downloads/moment.js"></script>
    <script language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css">

    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
    <style>
        @font-face {
            font-family: 'Myanmar3';
            src: url('{{storage_path("/fonts/Myanmar3.ttf")}}')  format('truetype');
        }
        body{
            font-family: 'Myanmar3', sans-serif;
        }
    </style>

</head>
<body>
<div class="wrapper">
    <div class="sidebar">
        <div class="sidebar-top">
            <p>My School</p>
        </div>

        <div class="sidebar-middle">
            <i class="fa fa-stumbleupon-circle"></i>
            <p>School Administrator</p>
        </div>

        <div class="sidebar-bottom">
            <ul class="sidebar-inside">
                <a href="{{route('admin.index')}}"><li><i class="fa fa-user"></i><p>Admin</p></li></a>
                <a href="{{route('batch.index')}}"><li><i class="fa fa-graduation-cap"></i><p>Batch</p></li></a>
                <a href="{{route('subject.index')}}"><li><i class="fa fa-book"></i><p>Subject</p></li></a>
                <a href="{{route('student.index')}}"><li><i class="fa fa-child"></i><p>Student</p></li></a>
                {{--<a href="{{route('year.index')}}"><li><i class="fa fa-child"></i><p>Year</p></li></a>--}}
                <a href="{{route('major.index')}}"><li><i class="fa fa-child"></i><p>Major</p></li></a>
                <a href="{{route('exam.index')}}"><li><i class="fa fa-child"></i><p>Exam</p></li></a>
                {{--<a href="{{route('mark.index')}}"><li><i class="fa fa-child"></i><p>Mark</p></li></a>--}}
                <a href="{{route('category.index')}}"><li><i class="fa fa-child"></i><p>Subject Category</p></li></a>
            </ul>
        </div>
    </div>

    <div class="nav-bar nav">
        <div class="burger">
            <div class="burger-inside">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div class="notification">
            <a href="{{route('adminlogin.logout')}}"><i class="fa fa-sign-out"></i></a>
            {{--<a href=""><i class="fa fa-envelope"></i></a>--}}
        </div>

        {{--<div class="dropdown">--}}
            {{--<a href="{{route('adminlogin.logout')}}"><i class="fa fa-sign-out"></i></a>--}}
            {{--<a href="" class="primary dropdown-toggle" type="button" data-toggle="dropdown">Admin--}}
                {{--<span class="caret"></span></a>--}}
            {{--<ul class="dropdown-menu">--}}
                {{--<li><a href="#">tsm</a></li>--}}
                {{--<li><a href="#">pp</a></li>--}}
                {{--<li><a href="#">tzwk</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    </div>

    <div class="content">
        @yield('content')
    </div>
</div>
@yield('models');
<div class="img_print">
    @yield('img')
</div>
@yield('script')
</body>
<script>
    $(document).ready(function() {
        $('.datepick').datetimepicker({
            format: "Y/M/D",
        });
    });
</script>
</html>