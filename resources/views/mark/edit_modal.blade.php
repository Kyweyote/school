<div class="add-path">
    <div class="modal fade" id="modal_editmark">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Mark</h4>
                </div>

                <form method="post" id="editurl">
                    {{csrf_field()}}
                    <div class="for-year">
                        {{--<div class="form-group modal-inside dropdown" id="name">--}}
                            {{--<label for="name">Roll No</label>--}}
                            {{--<input type="text" class="name-input dropdown-toggle" data-toggle="dropdown" id="mark_roll_no" name="searchRoll" value="{{old('searchRoll')}}" autocomplete="off">--}}
                            {{--<ul class="dropdown-menu" id="roll-edit-menu">--}}
                                {{--@foreach($students as $student)--}}
                                    {{--@php($year=\App\Models\Year::find($student->year_id))--}}
                                        {{--@php($year_name=$year->name)--}}
                                            {{--<li class="dropdown-item edit-roll" data-edit-year="{{$year_name}}" data-edit-name="{{$student->name}}">{{$student->roll_no}}</li>--}}
                                            {{--@endforeach--}}
                            {{--</ul>--}}
                        {{--</div>--}}

                        {{--<div class="form-group modal-inside" id="name">--}}
                            {{--<label for="student_name">Student</label>--}}
                            {{--<input type="text" id="mark_student_name" class="static" name="student_name" readonly>--}}
                        {{--</div>--}}

                        {{--<div class="form-group modal-inside" id="name">--}}
                            {{--<label for="name">Year</label>--}}
                            {{--<input type="text" id="mark_year" class="static" name="year_name" readonly>--}}
                        {{--</div>--}}
                        <input type="hidden" name="student_id" value="{{$students->id}}">

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Year</label>
                            <select class="form-group" name="year_id" id="mark_year_id">
                                @foreach($years as $year)
                                    <option value="{{$year->id}}" class="{{$year->id}}">{{$year->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Subject Category</label>
                            <select class="form-group" name="category_id" id="edit_category_id">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" class="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Subject</label>
                            <select class="form-group" name="subject_id" id="edit_subject_id">
                                @foreach($subjects as $subject)
                                    <option value="{{$subject->id}}" class="{{$subject->category_id}}">{{$subject->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Exam</label>
                            <select class="form-group" name="exam_id" id="mark_exam_id">
                                @foreach($exams as $exam)
                                    <option value="{{$exam->id}}">{{$exam->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Mark</label>
                            <input type="text" class="name-input" id="mark_mark" name="mark" value="{{old('mark')}}">
                            @if($errors->has('mark'))
                                <span class="error">{{$errors->first('mark')}}</span>
                            @endif
                        </div>


                        <div class="modal-save">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

