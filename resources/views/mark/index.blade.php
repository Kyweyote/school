@extends('masteradmin')
@section('title','Mark')
@section('content')

    <div class="list-of-class">
        <p>List of class Standard-1 Students</p>
    </div>

    <div class="main-content">
        <div class="select-class">
            <label>Select Class Name</label>
            <select>
                <option>Standard-1</option>
                <option>Standard-2</option>
                <option>Standard-3</option>
                <option>Standard-4</option>
            </select>
        </div>

        <div class="show-entries">
            <label>Show</label>
            <select>
                <option>10</option>
                <option>10</option>
                <option>10</option>
                <option>10</option>
                <option>10</option>
                <option>10</option>
                <option>10</option>
                <option>10</option>
                <option>10</option>
                <option>10</option>
                <option>10</option>
                <option>10</option>
            </select>
        </div>

        <div class="add-student">
            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal_addmark">
                <i class="fa fa-plus"></i>&nbspAdd Mark
            </button>
        </div>

        <div class="dropdown print">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-print"></i>Print
                <span class="caret"></span></button>
            <ul class="dropdown-menu printdropdown">
                <li><a href="{{route('excel.downloadExcel')}}">Excel</a></li>
                <li><a href="{{route('pdf.downloadPdf',['id'=>1])}}">PDF</a></li>
                <li><a href="#">A6</a></li>
            </ul>
        </div>

        <div class="search-box">
            <label>Search</label>
            <input class="search">
        </div>

        <table class="table-bordered table-striped table">
            <tr>
                <th><input type="checkbox"></input></th>
                <th>No</th>
                <th>Name</th>
                <th><select>
                        <option>Select Action</option>
                        <option>Select</option>
                        <option>Select</option>
                        <option>Select</option>
                    </select>
                </th>
            </tr>
            @php($no=1)
                @foreach($marks as $mark)
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>{{$no++}}</td>
                        <td>{{$mark->student_id}}</td>
                        @php($id=$mark->student_id)
                        @php($student=\App\Models\Student::find($id))
                        @php($year=\App\Models\Year::find($student->year_id))
                        <td><button class="detail" data-excelurl=""><i class="fa fa-eye"></i></button>
                            <button class="delete" data-toggle="modal" data-target="#modal_deletemark" data-deleteurl="{{route('mark.delete',['id'=>$mark->id])}}"><i class="fa fa-trash-o"></i></button>
                            <button class="edit" data-toggle="modal" data-target="#modal_editmark" data-year="{{$year}}" data-student="{{$student}}" data-mark="{{$mark}}" data-editurl="{{route('mark.edit',['id'=>$mark->id])}}"><i class="fa fa-pencil"></i></button>
                        </td>
                    </tr>
                @endforeach

        </table>

        <p>Showing 1 to 10 of 17 entries</p>

        <div class="page">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">Next</a></li>
            </ul>
        </div>
    </div>
@endsection
@section('models')
    @include('mark.create_modal')
    @include('mark.edit_modal')
    @include('mark.delete_modal')
    @endsection
@section('script')
    <script src="{{asset('js/mark.js')}}"></script>
@endsection