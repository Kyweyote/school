<div class="add-path">
    <div class="modal fade" id="modal_pdf">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Choose For PDF</h4>
                </div>

                <form method="post" action="{{route('pdf.downloadPdf',['id'=>$students->id])}}">
                    {{csrf_field()}}
                    <div class="for-year">

                        <input type="hidden" name="student_id" value="{{$students->id}}">

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Year</label>
                            <select class="form-group" name="year_id" id="year_id">
                                <option value="0">All Years</option>
                                @foreach($years as $year)
                                    <option value="{{$year->id}}" class="{{$year->id}}">{{$year->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Exam</label>
                            <select class="form-group" name="exam_id">
                                <option value="0">All Exam</option>
                                @foreach($exams as $exam)
                                    <option value="{{$exam->id}}">{{$exam->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group modal-inside" id="name">
                            <label for="name">Subject</label>
                            <select class="form-group" name="subject_id" id="subject_id">
                                <option value="0">All Subject</option>
                                @foreach($subjects as $subject)
                                    <option value="{{$subject->id}}" class="{{$subject->category_id}}">{{$subject->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="modal-save">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

