<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous"> -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.css">

    <link href="https://fonts.googleapis.com/css?family=Oswald:300" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
</head>
<body>
<div class="login-form">
    <form method="post" action="{{route('adminlogin.login')}}">
        {{csrf_field()}}
        <h3>Login</h3>

        <div class="login-footer">
            <span></span>
            <div class="login-control">
                <input type="text" class="login-box" id="email" placeholder="Email" name="email" value="{{old('name')}}">
                @if($errors->has('email'))
                    <span class="error">{{$errors->first('email')}}</span>
                @endif
            </div>

            <div class="login-control">
                <input type="password" class="login-box" id="password" placeholder="Password" name="password" value="{{old('password')}}">
                @if($errors->has('password'))
                    <span class="error">{{$errors->first('password')}}</span>
                    @endif
            </div>

            <div class="login-control">
                <button>Login</button>
            </div>
        </div>
        <p><a href="#"></a></p>
    </form>
</div>
</body>
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</html>