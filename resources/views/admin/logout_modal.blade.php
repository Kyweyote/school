<div class="modal fade" id="Logout_Modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-logout">
            <div class="modal-header">
                <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are You Sure?</h4>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span> -->
            </div>
            <form action="{{route('adminlogin.logout')}}">
            <div class="logout-modal">
                <!-- <div class="modal-footerdelete"> -->
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel
                </button>
                <button type="submit" class="btn btn-primary">Ok</button>
                <!-- </div> -->
            </div>
            </form>
        </div>

    </div>
</div>