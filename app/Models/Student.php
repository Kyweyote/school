<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name',
        'roll_no',
        'code',
        'date_of_birth',
        'father_name',
        'mother_name',
        'town',
        'address',
        'nrc',
        'admit_date',
        'batch_id',
        'major_id',
        'year_id',
        'photo',
    ];
    public function phones(){
        return $this->hasMany('App\Models\Phone');
    }
    public function marks(){
        return $this->hasMany('App\Models\Mark');
    }
    public function years(){
        return $this->hasMany('App\Models\Year');
    }
    public function major(){
        return $this->belongsTo('App\Models\Major');
    }
}
