<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $fillable=[
        'ph_no',
        'student_id',
    ];
    public function student(){
        return $this->belongsTo('App\Models\Student');
    }
}
