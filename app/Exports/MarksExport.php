<?php

namespace App\Exports;

use App\Models\Exam;
use App\Models\Mark;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Year;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MarksExport implements FromCollection,ShouldAutoSize,WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;
    protected $exporter;
    protected $year;
    protected $major;
    public function __construct($year,$major)
    {
        $this->year = $year;
        $this->major = $major;
    }
    public function collection()
    {
        $year = $this->year;
        $major = $this->major;
        $data = array();
        $no = 1;
        $students = Student::where('major_id',$major)->get();
        foreach ($students as $student){
            $marks = Mark::where('year_id',$year)->where('student_id',$student->id)->get();
            foreach($marks as $mark){
                $student_roll = $student->roll_no;
                $student_name = $student->name;
                $subject = Subject::find($mark->subject_id);
                $subject_name = $subject->name;
                $exam = Exam::find($mark->exam_id);
                $exam_name = $exam->name;
                $mark = $mark->mark;
                array_push($data,array($no++,$student_roll,$student_name,$subject_name,$exam_name,$mark));
            }
        }
        return collect($data);
    }
    public function headings(): array
    {
        return [
            '#',
            'Roll No',
            'Name',
            'Subject',
            'Exam',
            'Mark',
        ];
    }
}
