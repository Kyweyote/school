<?php

namespace App\Http\Middleware;

use Closure;

class AdminChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('admincheck')){
            return redirect()->route('adminlogin.index');
        }
        return $next($request);
    }
}
