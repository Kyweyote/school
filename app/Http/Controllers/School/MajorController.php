<?php

namespace App\Http\Controllers\school;

use App\Models\Major;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MajorController extends Controller
{
    public function index(){
        $majors=Major::all();
        return view('major.index',compact('majors'));
    }
    public function create(Request $request){
        $validator=Validator::make($request->all(),[
           'name'=>'required|string|max:255',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        Major::create([
            'name'=>$request->get('name'),
        ]);
        return redirect()->back();
    }
    public function update(Request $request,$id){
        $validator=Validator::make($request->all(),[
            'name'=>'required|string|max:255',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $majors=Major::find($id);
        $majors->name=$request->get('name');
        $majors->update();
        return redirect()->back();
    }
    public function delete($id){
        $majors=Major::find($id);
        $majors->delete();
        return redirect()->back();
    }
}
