<?php

namespace App\Http\Controllers\school;

use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;

class SubjectController extends Controller
{
    public function index(){
        $subjects = Subject::all();
        $categories = Category::all();
        return view('subject.index',compact('subjects','categories'));
    }
    public function create(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=>'required|string|max:255',
            'code'=>'required|string|max:255',
            'category'=>'required',
            'credit'=>'required|integer',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        Subject::create([
            'name'=>$request->get('name'),
            'code'=>$request->get('code'),
            'credit'=>$request->get('credit'),
            'category_id'=>$request->get('category'),
        ]);
        return redirect()->back();
    }
    public function update(Request $request,$id){
        $validator = Validator::make($request->all(),[
            'name'=>'required|string|max:255',
            'code'=>'required|string|max:255',
            'category'=>'required',
            'credit'=>'required',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $subject = Subject::find($id);
        $subject->name = $request->get('name');
        $subject->code = $request->get('code');
        $subject->credit = $request->get('credit');
        $subject->category_id = $request->get('category');
        $subject->update();
        return redirect()->back();
    }
    public function delete($id){
        $subject = Subject::find($id);
        $subject->delete();
        return redirect()->back();
    }

}
