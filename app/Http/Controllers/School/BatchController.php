<?php

namespace App\Http\Controllers\school;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Batch;
use Illuminate\Support\Facades\Validator;

class BatchController extends Controller
{
    public function index(){
        $batches = Batch::all();
        return view('batch.index',compact('batches'));
    }
    public function create(Request $request){
        $validator=Validator::make($request->all(),[
           'name'=>'required|string|max:255',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        Batch::create([
            'name'=>$request->get('name'),
        ]);
        return redirect()->back();
    }
    public function update(Request $request,$id){
        $validator=Validator::make($request->all(),[
            'name'=>'required|string|max:255',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $batches=Batch::find($id);
        $batches->name=$request->get('name');
        $batches->update();
        return redirect()->back();
    }
    public function delete($id){
        $batches=Batch::find($id);
        $batches->delete();
        return redirect()->back();
    }
}
