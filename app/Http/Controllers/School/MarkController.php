<?php

namespace App\Http\Controllers\school;

use App\Models\Category;
use App\Models\Exam;
use App\Models\Mark;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Year;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MarkController extends Controller
{
    public function index(){
        $marks = Mark::all();
        $students = Student::all();
        $subjects = Subject::all();
        $exams = Exam::all();
        $categories = Category::all();
        return view('mark.index',compact('marks','students','exams','categories','subjects'));
    }
    public function create(Request $request){
        $validator = Validator::make($request->all(),[
            'year_id'=>'required',
            'subject_id'=>'required',
            'exam_id'=>'required',
            'mark'=>'required',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
//        $student=Student::where('roll_no',$request->get('searchRoll'))->first();
//        $student_id=$student->id;
//        $year = Year::where('name',$request->get('year_name'))->first();
//        $year_id = $year->id;
        Mark::create([
            'student_id'=>$request->get('student_id'),
            'subject_id'=>$request->get('subject_id'),
            'exam_id'=>$request->get('exam_id'),
            'year_id'=>$request->get('year_id'),
            'mark'=>$request->get('mark'),
        ]);
        return redirect()->back();
    }
    public function update(Request $request,$id){
        $validator = Validator::make($request->all(),[
            'student_id'=>'required',
            'subject_id'=>'required',
            'exam_id'=>'required',
            'year_id'=>'required',
            'mark'=>'required',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $mark = Mark::find($id);
        $mark->student_id=$request->get('student_id');
        $mark->subject_id = $request->get('subject_id');
        $mark->exam_id = $request->get('exam_id');
        $mark->year_id = $request->get('year_id');
        $mark->mark = $request->get('mark');
        $mark->update();
        return redirect()->back();
    }
    public function delete($id){
        $mark = Mark::find($id);
        $mark->delete();
        return redirect()->back();
    }
    public function detail($id){
        $students = Student::find($id);
        $years = Year::all();
        $subjects = Subject::all();
        $exams = Exam::all();
        $categories = Category::all();
//        $years = Mark::where('student_id',$students->id)->Distinct()->get(['year_id']);
        $marks = Mark::where('student_id',$students->id)->orderBy('year_id')->orderBy('exam_id')->paginate(8);
        return view('student.detail',compact('students','marks','years','subjects','exams','categories'));
    }

}
