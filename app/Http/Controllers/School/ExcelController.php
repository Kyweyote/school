<?php

namespace App\Http\Controllers\school;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExcelController extends Controller
{
    public function getexcel(Request $request){
        $year = $request->get('year');
        $major = $request->get('major');
        return redirect('excel.downloadExcel',compact('year','major'));
    }
}
