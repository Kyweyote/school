<?php

namespace App\Http\Controllers\school;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();
        return view('category.index',compact('categories'));
    }
    public function create(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'required|string|max:255',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        Category::create([
            'name'=>$request->get('name'),
        ]);
        return redirect()->back();
    }
    public function update(Request $request,$id){
        $validator=Validator::make($request->all(),[
            'name'=>'required|string|max:255',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $categories = Category::find($id);
        $categories->name=$request->get('name');
        $categories->update();
        return redirect()->back();
    }
    public function delete($id){
        $categories = Category::find($id);
        $categories->delete();
        return redirect()->back();
    }
}
