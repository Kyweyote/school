<?php

namespace App\Http\Controllers\school;

use App\Models\Batch;
use App\Models\Major;
use App\Models\Mark;
use App\Models\Phone;
use App\Models\Student;
use App\Models\Year;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    public function index(){
        $students = Student::with('major')->paginate(8);
        $batches = Batch::all();
        $majors = Major::all();
        $years = Year::all();
        return view('student.index',compact('students','batches','majors','years'));
    }
    public function create(Request $request){
//        return $request->all();
        $validator = Validator::make($request->all(),[
            'name'=>'required|string|max:255',
            'roll_no'=>'required|string|max:255',
            'code'=>'required|string|max:255',
            'date_of_birth'=>'required|string|max:255',
            'father_name'=>'required|string|max:255',
            'mother_name'=>'required|string|max:255',
            'town'=>'required|string|max:255',
            'address'=>'required|string|max:255',
            'nrc'=>'required|string|max:255',
            'ph_no'=>'required|string|max:255',
            'admit_date'=>'required',
            'batch_id'=>'required|integer',
            'major_id'=>'required|integer',
            'year_id'=>'required|integer',
            'photo'=>'required|image',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $photo = $request->file('photo');
        if (!empty($photo)) {
            $filename = uniqid() . '_' . $photo->getClientOriginalName();
            Storage::put('/uploads/' . $filename, file_get_contents($photo));
        }
        $id = DB::table('students')->insertGetId(array(
            'name'=>$request->get('name'),
            'roll_no'=>$request->get('roll_no'),
            'code'=>$request->get('code'),
            'date_of_birth'=>$request->get('date_of_birth'),
            'father_name'=>$request->get('father_name'),
            'mother_name'=>$request->get('mother_name'),
            'town'=>$request->get('town'),
            'address'=>$request->get('address'),
            'nrc'=>$request->get('nrc'),
            'admit_date'=>$request->get('admit_date'),
            'batch_id'=>$request->get('batch_id'),
            'major_id'=>$request->get('major_id'),
            'year_id'=>$request->get('year_id'),
            'photo'=>$filename,
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now(),
        ));

        $phones = $request->get('ph_no');
        $phone =explode(',',$phones);
        $student = Student::find($id);
        foreach ($phone as $ph){
            $student->phones()->create([
                'ph_no'=>$ph,
            ]);
        }
        return redirect()->back();
    }
    public function update(Request $request,$id){
        $validator = Validator::make($request->all(),[
            'name'=>'required|string|max:255',
            'roll_no'=>'required|string|max:255',
            'code'=>'required|string|max:255',
            'date_of_birth'=>'required|string|max:255',
            'father_name'=>'required|string|max:255',
            'mother_name'=>'required|string|max:255',
            'town'=>'required|string|max:255',
            'address'=>'required|string|max:255',
            'nrc'=>'required|string|max:255',
            'admit_date'=>'required',
            'ph_no'=>'required|string|max:255',
            'batch_id'=>'required|integer',
            'major_id'=>'required|integer',
            'year_id'=>'required|integer',
        ]);
        if($validator->fails()){
//            return $request->all();
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $student = Student::find($id);
        $student->name =$request->get('name');
        $student->roll_no = $request->get('roll_no');
        $student->code = $request->get('code');
        $student->date_of_birth = $request->get('date_of_birth');
        $student->father_name = $request->get('father_name');
        $student->mother_name=$request->get('mother_name');
        $student->town = $request->get('town');
        $student->address = $request->get('address');
        $student->nrc = $request->get('nrc');
        $student->admit_date = $request->get('admit_date');
        $student->batch_id = $request->get('batch_id');
        $student->major_id = $request->get('major_id');
        $student->year_id = $request->get('year_id');
        $file = $request->file('photo');
        $oldphoto = $student->photo;
        if(!empty($file)){
            $filename = uniqid().'_'.$file->getClientOriginalName();
            Storage::put('/uploads/'.$filename,file_get_contents($file));
            $student->photo = $filename;
            unlink(storage_path('app/uploads/'.$oldphoto));
        }
        else{
            $student->photo = $oldphoto;
        }
        $student->update();
        $phone = Phone::where('student_id',$id)->get();
        foreach($phone as $phone_arr){
            $phone_arr->delete();
        }
        $phones = $request->get('ph_no');
        $phonearr =explode(',',$phones);
        foreach ($phonearr as $ph){
            $student->phones()->create([
                'ph_no'=>$ph,
            ]);
        }
        return redirect()->back();
    }
    public function delete($id){
        $student = Student::find($id);
        $student->delete();
        $oldimage = $student->photo;
        unlink(storage_path('app/uploads/' . $oldimage));
        return redirect()->back();
    }
    public function getcard($id){
        $student = Student::find($id);
        $major_id = $student->major_id;
        $major = Major::find($major_id);
        return view('student.card',compact('student','major'));
    }
    public function getImage($name){
        return response()->file(storage_path().'/app/uploads/'.$name);
    }
    public function search(Request $request){
        $batches = Batch::all();
        $majors = Major::all();
        $years = Year::all();
        $word= $request->get('word');
        $students = Student::where('name','like','%'.$word.'%')->paginate(8);
        return view('student.index',compact('students','batches','years','majors'));
    }

}
