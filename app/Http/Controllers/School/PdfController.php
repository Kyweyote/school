<?php

namespace App\Http\Controllers\school;

use App\Models\Exam;
use App\Models\Mark;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Validator;
use BeyondPlus\TCPDF\Facades\MMTCPDF as MMTCPDF;

class PdfController extends Controller
{
    public function getPdf(Request $request,$id){
        $validator = Validator::make($request->all(),[
            'date'=>'required',
            'uni'=>'required',
            'rector'=>'required',
            'position'=>'required',
            'signature_date'=>'required',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
//        $date = $request->get('date');
//        $uni =$request->get('uni');
//        $rector = $request->get('rector');
//        $position = $request->get('position');
//        $signature_date = $request->get('signature_date');
//        $student = Student::find($id);
//        $orders = Mark::where('student_id',$student->id)->orderBy('year_id')->orderBy('exam_id')->get();
//        $orders_count = Mark::where('student_id',$student->id)->orderBy('year_id')->orderBy('exam_id')->count();
////        $fontname = MMTCPDF::font('Zawgyi-One');
////        $uni=\Rabbit::uni2zg($request->get('uni'));
//        $fontname = MMTCPDF::font('Myanmar3');
//        MMTCPDF::SetFont($fontname , 11);
//        MMTCPDF::SetTitle('Record Report');
//        MMTCPDF::AddPage('P','A4');
//        MMTCPDF::writeHtml(view('pdf.student',compact('orders_count','student','orders','date','uni','rector','position','signature_date')));
//        MMTCPDF::Output('report.pdf', 'I');
        $date = $request->get('date');
        $uni = $request->get('uni');
//        $uni =\Rabbit::uni2zg($request->get('uni'));
        $address = $request->get('address');
        $ph_no = $request->get('ph_no');
        $rector = $request->get('rector');
//        $rector = \Rabbit::uni2zg($request->get('rector'));
        $position = $request->get('position');
        $signature_date = $request->get('signature_date');
        $student = Student::find($id);
        $student_name = $student->name;
//        $student_name = \Rabbit::uni2zg($student->name);
        $orders = Mark::where('student_id',$student->id)->orderBy('year_id')->orderBy('exam_id')->get();
        $orders_count = Mark::where('student_id',$student->id)->orderBy('year_id')->orderBy('exam_id')->count();
        set_time_limit(300);
//        return view('pdf.student',compact('orders_count','student','orders','date','uni','rector','position','signature_date'));
        $pdf = PDF::loadView('pdf.student',compact('student','orders','date','uni','address','ph_no','rector','position','signature_date','orders_count','student_name'));
        return $pdf->download('student.pdf');
    }
}
