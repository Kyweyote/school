<?php

namespace App\Http\Controllers\school;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')
            ->except('showLogin', 'login' , 'logout');

    }

    public function showLogin(){
        return view('admin.login');
    }

    public function login(Request $request){
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required'
        ]);

        $user = Admin::where('email','=',$request->email)->first();
        if($user){
            if(Hash::check($request->password,$user->password)){
                session()->put('admincheck',$user->email);
                return redirect()->route('admin.index');
            }
        }
        return redirect()->back();

    }

    public function logout(){
        session()->forget('admincheck');
        return redirect()->route('adminlogin.index');
    }
    public function index(){
        $admins = Admin::all();
        return view('admin.index',compact('admins'));
    }
    public function create(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=>'required|string|max:255',
            'email'=>'required|email|string|max:255|unique:admins,email',
            'password'=>'required|min:6',
            'password_confirmation'=>'required_with:password|same:password|min:6',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        Admin::create([
            'name'=>$request->get('name'),
            'email'=>$request->get('email'),
            'password'=>bcrypt($request->get('password')),
        ]);
        return redirect()->back();
    }
    public function update(Request $request,$id){
        $validator = Validator::make($request->all(),[
            'name'=>'required|string|max:255',
            'email'=>"required|string|email|max:255|unique:admins,email,$id",
        ]);
        if($validator->fails()){
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $admins = Admin::find($id);
        $admins->name = $request->get('name');
        $admins->email = $request->get('email');
        $admins->update();
        return redirect()->back();
    }
    public function delete($id){
        $admin = Admin::find($id);
        $admin->delete();
        return redirect()->back();
    }
}
