<?php

namespace App\Http\Controllers\school;

use App\Models\Year;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class YearController extends Controller
{
    public function index(){
        $years=Year::all();
        return view('year.index',compact('years'));
    }
    public function create(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'required|string|max:255',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        Year::create([
            'name'=>$request->get('name'),
        ]);
        return redirect()->back();
    }
    public function update(Request $request,$id){
        $validator=Validator::make($request->all(),[
            'name'=>'required|string|max:255',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $years=Year::find($id);
        $years->name=$request->get('name');
        $years->update();
        return redirect()->back();
    }
    public function delete($id){
        $years=Year::find($id);
        $years->delete();
        return redirect()->back();
    }
}
