<?php

namespace App\Http\Controllers\school;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Exam;
use Illuminate\Support\Facades\Validator;

class ExamController extends Controller
{
    public function index(){
        $exams = Exam::all();
        return view('exam.index',compact('exams'));
    }
    public function create(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'required|string|max:255',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        Exam::create([
            'name'=>$request->get('name'),
        ]);
        return redirect()->back();
    }
    public function update(Request $request,$id){
        $validator=Validator::make($request->all(),[
           'name'=>'required|string|max:255',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $exams=Exam::find($id);
        $exams->name=$request->get('name');
        $exams->update();
        return redirect()->back();
    }
    public function delete($id){
        $exams=Exam::find($id);
        $exams->delete();
        return redirect()->back();
    }
}
