<?php

namespace App\Http\Controllers\school;

use App\Exports\MarksExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MaatwebsiteController extends Controller
{
    public function downloadExcel(Request $request)
    {
        $year = $request->get('year');
        $major = $request->get('major');
        $export = app()->makeWith(MarksExport::class,compact('year','major'));
        return $export->download( 'users.xlsx');
//        return Excel::download(new MarksExport(), 'users.xlsx');
    }
}
